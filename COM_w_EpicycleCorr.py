#!/usr/bin/env python

"""
Analyze the run(s) assuming that the COM motion
has both a linear and epicycle drift. The epicycles
are analyzed and removed from the input data, and then
the COM motion should be purely linear. This linear COM 
is processed by moble/scri and the waveforms are corrected
accordingly.

Removing the epicycles before using scri to estimate the
linear component is anticipated to improve the accuracy
of the translation and boost esitmation values, and hence
improve the waveforms overall.

Inputs are the names of the directories to start looking for
Horizons.h5 files. Waveform files to correct are assumed to 
be in the same directory as the Horizons.h5 file.

Note that there are no output plots or files besides the 
COM corrected waveforms and data. To perfom an analysis, 
please run COMEpicycleAnalysis.py.
"""

import sys
import os
import math
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import scipy.integrate as integrate
import h5py
import scri
import argparse
import re

def DataLoader(h5location):
    #Given the h5 file location for the data, open the
    #h5 file and load the positions, masses, times of
    #the 2 black holes
    
    h5file = h5py.File(h5location,'r')
    xa = h5file['/AhA.dir/CoordCenterInertial.dat']
    xb = h5file['/AhB.dir/CoordCenterInertial.dat']
    mass_a = h5file['AhA.dir/ChristodoulouMass.dat'][:,1]
    mass_b = h5file['AhB.dir/ChristodoulouMass.dat'][:,1]
    
    return xa,xb,mass_a,mass_b,xa[:,0]

def NewtonianC(xa,xb,ma,mb):
    #Defines the Newtonian COM, c = c1+c2
    Nc_x = []
    Nc_y = []
    Nc_z = []
    for idx in range(0,len(xa)):
        Nc_x = np.append(Nc_x,(ma[idx]*xa[idx,1] + mb[idx]*xb[idx,1])/(ma[idx]+mb[idx]))
        Nc_y = np.append(Nc_y,(ma[idx]*xa[idx,2] + mb[idx]*xb[idx,2])/(ma[idx]+mb[idx]))
        Nc_z = np.append(Nc_z,(ma[idx]*xa[idx,3] + mb[idx]*xb[idx,3])/(ma[idx]+mb[idx]))
        
    return np.column_stack((Nc_x,Nc_y,Nc_z))

def MagC(x):
    #Computes the magnitude of the input vector, assuming Nx3 shape
    C = []
    for idx in range(0,len(x)):
        C = np.append(C,math.sqrt(x[idx,0]*x[idx,0] + x[idx,1]*x[idx,1] + x[idx,2]*x[idx,2]))
    return C

def Compute_OrbitalFrequency(xA, xB):
    #Taken from SpEC/Support/bin/BbhDiagnosticsImpl.py
    #Changed default fit to Spline, from Fit
    #Took out unnecessary functions to cut down line space

    def SplineData(data):
        from scipy.interpolate import splrep,splev
        t   = data[:,0]
        spline_x = splrep(t, data[:,1])
        spline_y = splrep(t, data[:,2])
        spline_z = splrep(t, data[:,3])
        dx = splev(t, spline_x, der=1)
        dy = splev(t, spline_y, der=1)
        dz = splev(t, spline_z, der=1)
        v = np.vstack((t,dx,dy,dz)).T
        return data, v
    
    xA_fit,vA=SplineData(xA)
    xB_fit,vB=SplineData(xB)

    # Compute Orbital frequency (r x dr/dt)/r^2
    t  = xA_fit[:,0]
    dr = xA_fit[:,1:] - xB_fit[:,1:]
    dr2 = MagC(dr)**2   #changed from original SpEC function
    dv = vA[:,1:] - vB[:,1:]
    Omega = np.cross(dr,dv)/dr2[:,np.newaxis]
    return Omega

def UnitVectors(xa,xb):
    #Define the rotating unit vectors of the system
    #Given the 4-vector positions of the BBH
    sep = xa[:,[1,2,3]] - xb[:,[1,2,3]]

    n_hat = sep/MagC(sep)[:,np.newaxis]

    Ofreq = Compute_OrbitalFrequency(xa, xb)
    k_hat = Ofreq/MagC(Ofreq)[:,np.newaxis]

    lambda_hat = np.cross(-n_hat, k_hat)
    return n_hat, k_hat, lambda_hat

def Dot(veca, vecb):
    #take the dot product of each time index of the vectors veca, vecb
    #and store in a returned 1D vector. Assume 3D vector input
    
    d = []
    
    for idx in range(0,len(veca)):
        d = np.append(d, np.dot(veca[idx],vecb[idx]))
        
    return d

def EpicycleCorrection(n,l,ddotn,ddotl):
    #Apply the epicycle correction factor to the COM data,
    #given n,l,ddotn,ddotl
    
    return (np.multiply(n.T, ddotn) + np.multiply(l.T, ddotl)).T

def main():

    #Parse arguments from command line
    parser=argparse.ArgumentParser(description=__doc__)

    parser.add_argument("--dir",nargs="+",required="True",
                        help="Locations of Horizons.h5 files to consider.")

    args = parser.parse_args()

    h5files = ['rhOverM_Asymptotic_GeometricUnits.h5','rMPsi4_Asymptotic_GeometricUnits.h5']
    extractions = ['Extrapolated_N2.dir','Extrapolated_N3.dir','Extrapolated_N4.dir','OutermostExtraction.dir']
    
    for directory in args.dir:
        #Find all the Horizons.h5 files in the specified directory.
        in_files=[os.path.join(d,x)
                  for d, dirs, files in os.walk(directory, topdown=True)
                  for x in files if x.endswith('Horizons.h5')]


        for in_file in sorted(in_files):
            #For each Horizons.h5 file that we have in the specified directory:

            x_a,x_b,mass_a,mass_b,times = DataLoader(in_file) #load data

            metaname=in_file[:-11]+'metadata.txt'  #Find the relaxed time to calculate beginning fraction
            with open(metaname,'r') as metafile:
                for line in metafile:
                    if 'relaxed-measurement-time = ' in line:
                        tryfloat=[]
                        for s in line.split():
                            try:
                                tryfloat.append(float(s))
                            except ValueError:
                                pass
                        t_relaxed=tryfloat[0]

            skip_beginning_fraction=2*t_relaxed/len(times) #Define beginning fraction and ending fraction
            skip_ending_fraction = 0.1

            i_i, i_f = int(len(times)*skip_beginning_fraction), int(len(times)*(1.0-skip_ending_fraction)) #Find associated times
 
            #First guess for boost+translation
            x_i,v_i,t_i,t_f = scri.SpEC.estimate_avg_com_motion(in_file,skip_beginning_fraction,skip_ending_fraction)
            
            Nc = NewtonianC(x_a,x_b,mass_a,mass_b) #Calculate newtonian COM and magnitude
            NC = MagC(Nc)

            n_hat,k_hat,lambda_hat = UnitVectors(x_a,x_b) #Find the rotating unit vectors

            #Compute the translation x_i and boost_v_i for the COM - Epicycles (from moble/scri/SpEC/com_motion.py)
            xtmp = np.array([0.0,0.0,0.0])
            vtmp = np.array([0.0,0.0,0.0])
            i=0

            while (np.linalg.norm(x_i-xtmp) >=1e-5 and np.linalg.norm(v_i-vtmp) >=1e-8): #Perform iteratively until x_i,v_i are constant
                xtmp=x_i  #Keep old value
                vtmp=v_i

                delta = Nc - np.array([x_i + v_i*t_j for t_j in times])
                ddotn = Dot(delta, n_hat)    #delta_n
                ddotl = Dot(delta,lambda_hat) #delta_lambda

                Ec = EpicycleCorrection(n_hat,lambda_hat,ddotn,ddotl) #Epicycle correction

                newCOM = Nc - Ec  #Newtonian COM - Epicycle correction
                
                v1,x1 = np.polyfit(times,newCOM[:,0],1)
                v2,x2 = np.polyfit(times,newCOM[:,1],1)
                v3,x3 = np.polyfit(times,newCOM[:,2],1)

                x_i = np.array([x1,x2,x3])
                v_i = np.array([v1,v2,v3])
                i=i+1

                #CoM_0 = integrate.simps(newCOM[i_i:i_f+1], times[i_i:i_f+1], axis=0) 
                #CoM_1 = integrate.simps((times[:, np.newaxis]*newCOM)[i_i:i_f+1], times[i_i:i_f+1], axis=0)

                #x_i = 2*(CoM_0*(2*t_f**3 - 2*t_i**3) + CoM_1*(-3*t_f**2 + 3*t_i**2))/(t_f - t_i)**4
                #v_i = 6*(CoM_0*(-t_f - t_i) + 2*CoM_1)/(t_f - t_i)**3
                #print(repr(x_i)+', '+repr(v_i)+'\n')

            #Correct waveforms with final translation and boost, save to new h5 file.
            print(repr(x_i)+', '+repr(v_i)+', '+repr(i)+'\n') 
            for h5file in h5files:
                for extraction in extractions:
                    w_m = scri.SpEC.read_from_h5(in_file[:-11]+h5file+'/'+extraction)
                    path_to_new_waveform=in_file[:-11]+h5file[:-3]+'_COMwE.h5/'+extraction 
                    w_m = w_m.transform(space_translation=x_i,boost_velocity=v_i)
                    scri.SpEC.write_to_h5(w_m,path_to_new_waveform,file_write_mode='a')

if __name__=="__main__":
    main()
