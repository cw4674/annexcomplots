Mass and Spin plots:

     These plots show the relationship between the COM position drift
     over all time (Avg) and the COM position drift at the time of 
     common horizon for every Lev and simulation. The Lev are colour
     coded, with red = Lev1, green = Lev2, blue = Lev3, black = Lev4,
     and magenta = Lev5. There are two different shaped markers for 
     the two different COM position drift values. The circles are the
     average (over all time for each Lev/point) COM position drift
     values and the squares are the COM position drift values at the
     time of common horizon.

     One can create spin and mass files with the python script
     COMPositionDrift_SpinAndMass.py. This script requires two flags,
     --dir [dir/containing/Horizons_and_metadata.txt] --filename
     [filename_for_output_files]

     The current files from the Simulation Annex are named: 

     COMPositionDrift_Mass_and_Spin.txt
     (Text output with simulation name, Lev, average over time of
     COM position drift, COM position drift at common horizon, 
     and corresponding spin and mass)

     COMPositionDrift_Spin.pdf 
     (plotted against the magnitude of relaxed-spin1 from metadata.txt)

     COMPositionDrift_Mass.pdf 
     (plotted against relaxed-mass1 from metadata.txt)

Velocity and Position Plots:

	 There are several plots that show relationships between the
	 COM velocity and position for the simulations. The quantities
	 used to describe these relationships are: 

	 1. the COM velocity per Lev in each simulation (vec{v}_k), which 
	 is the COM velocity estimated for each Lev in each simulation over 
	 the time the Lev has ran for. Note that one cannot acquire COM 
	 velocities at specific time points, so all of the COM velocities
	 are time averaged. 

	 2. the average COM velocity (vec{v}_avg), which is the average 
	 COM velocity for a simulation. This average is calculated by 
	 taking the COM velocities for each Lev in a simulation and 
	 averaging them. 
	 ie. vec{v}_avg = [sum(over i,from 0 to k) vec{v}_i]/k
	 where k is the total number of Levs in a simulation directory.

	 3. the deviance of the Lev COM velocity from the average COM
	 velocty. 
	 ie. |vec{v}_k - vec{v}_avg| for all k, where k is the Lev #.

	 4. the COM posiiton per Lev in each simulation, vec{p}_k. This is 
	 the same as 1., with the COM position being averaged over the total
	 run time for the Lev. Even though the COM positions can be found
	 for each time point (most notably at the common horizon time), 
	 the investigations for specific time COM positions are in the 
	 spin and mass plots (detailed above).

	 One can create plots for the COM velocity, COM position, average
	 COM velocity, COM velocity deviation, and the components of the 
	 average COM velocity (ie. in the x-y, y-z, x-z planes) by using
	 the script COMVelocityAndPositionDrift.py. The script requires 2
	 flags, --dir [path/to/Horizons.h5] --filename [filename/for/output].
	 To make the 2D x-y,y-z,x-z plots for the average COM velocity, 
	 include the flag --scatter. To make histogram plots for the 
	 average COM velocity, COM velocity and position per Lev, and COM
	 velocity deviation per Lev, include the flag --histogram.

	 The current files from the Simulation Annex are named:
	 
	 COMDrift_AvgCOMVelocity.pdf 
	 (histogram of average COM velocity)
	 **NOTE** Average COM velocity here means the COM velocity 
	 averaged over the Levs in the simulation. The COM velocity
	 values are always time averaged over the whole time of the
	 simulation.
	
	 COMDrift_AvgCOMVelocity.txt 
	 (Text output of average COM velocity)

	 COMDrift_AvgVelocity_xy.pdf 
	 (2D scatter plot of average COM velocity in the x-y plane)

	 COMDrift_AvgVelocity_xz.pdf
	 (2D scatter plot of average COM velocity in the x-z plane)
	 
	 COMDrift_AvgVelocity_yz.pdf
	 (2D scatter plot of average COM velocity in the y-z plane)

	 COMDrift_LevCOMPosition.pdf
	 (histogram of the COM positions per Lev)
	 
	 COMDrift_LevCOMVelocityAndPosition.txt
	 (text output for all COM position and velocity values, including
	 COM velocity deviation. All values are magnitudes)

	 COMDrift_LevCOMVelocityDeviation.pdf
	 (histogram of the COM velocity deviation from the average
	 COM velocity for every Lev)

	 COMDrift_LevCOMVelocity.pdf
	 (histogram of the COM velocities per Lev)



				   
