#!/usr/bin/env python

"""
Find the COM position drift at the common horizon time.
Will output a data file containing the name of the run and Lev, as 
well as the position drifts.

Outputs include:
mass vs v plot
spin plot
text output of data organized by Lev
"""

import os
import sys
import math
import argparse
import numpy as np
import matplotlib.pyplot as plt

def blockPrint():
#prevent printing to screen or to files
	sys.stdout = open(os.devnull, 'w')

def enablePrint():
#enable printing
	sys.stdout = sys.__stdout__

def main():

    #Parse Arguments from command line
    parser = argparse.ArgumentParser(
        description = "Find the time that a common horizon is detected in the binary and cacluate the COM drift at that time")
    parser.add_argument("--dir", nargs = "+", required = "True",
                        help = "Directory in which to start looking for the metadata.txt and Horizons.h5 files.")
    parser.add_argument("--filename", nargs = "+", required = "True", dest = "filename",
                        help = "Include a filename to make a plot of the spins and massof the BBH (relaxed-spin1, relaxed-mass1 in metadata.txt) vs the COM position drift. The option is the plot file name - do not include pdf!")

    args = parser.parse_args()
	
    import scri.SpEC as SpEC
    	
#Find and store all of the directories within the given directory that contain Horizons.h5.
#os.path.join combines the directory name d and file name x for simplicity when calling the 
#scri functions.
    h5dir = [os.path.join(d,x)
       	 for d, dirs, files in os.walk(args.dir[0], topdown=True)
       	 for x in files if x.endswith("Horizons.h5")]

    h5dir_lev1=[]
    h5dir_lev2=[]
    h5dir_lev3=[]
    h5dir_lev4=[]
    h5dir_lev5=[]

#Do the same for metadata.txt. Presumably, the same directories that contain Horizons.h5 will
#also contain metadata.txt
    metadir = []

    for x in range(0,len(h5dir)):
        metaname = h5dir[x]
        metaname = metaname[:-11]
        metadir.append(metaname+"metadata.txt")


#Create arrays to hold COM position at common horizon time, as well as the common horizon time
#itself from the metadata.txt files.
   # com_pos_common_horizon = []             #Taking out com position at horizon
   # com_pos_common_horizon_lev1 = []        #Plot velocity values instead
   # com_pos_common_horizon_lev2 = []
   # com_pos_common_horizon_lev3 = []
   # com_pos_common_horizon_lev4 = []
   # com_pos_common_horizon_lev5 = []
   # t_common_horizon = []

#Create arrays for spins, masses, and average COM position values, if these flags are included.\
    spins_lev1 = []
    spins_lev2 = []
    spins_lev3 = []
    spins_lev4 = []
    spins_lev5 = []
    avgcom_pos_lev1 = []
    avgcom_pos_lev2 = []
    avgcom_pos_lev3 = []
    avgcom_pos_lev4 = []
    avgcom_pos_lev5 = []
    avgcom_vel_lev1 = []
    avgcom_vel_lev2 = []
    avgcom_vel_lev3 = []
    avgcom_vel_lev4 = []
    avgcom_vel_lev5 = []
    mass_lev1 = []
    mass_lev2 = []
    mass_lev3 = []
    mass_lev4 = []
    mass_lev5 = []

#Run over all files that were in the specified starting directory
    for x in range(0,len(h5dir)):

        #Find the lines in metadata.txt that have the common horizon time data
        with open(metadir[x],'r') as metafile:
            for line in metafile:
                #if "common-horizon-time  = " in line:
                #    #Extract the time and assign as a float. Append to array of times. 
                #    tryfloat = []
                #    for s in line.split():
                #        try:
                #            tryfloat.append(float(s))
                #        except ValueError:
                #            pass
                #    time_comhorizon = tryfloat[0]
                #Find the value of the relaxed-spin1 element in 
                #metadata.txt and add the magnitude to an array of the spins.
		if "relaxed-dimensionless-spin1" in line:
			tryfloatspin = []
			for l in line.split():
                            try:
                                if l[-1]==',':
                                    tryfloatspin.append(float(l[:-1]))
                                else:
                                    tryfloatspin.append(float(l))
                            except ValueError:
                                pass
			if "Lev1" in h5dir[x]:
				spins_lev1.append(math.sqrt(pow(tryfloatspin[0],2)+pow(tryfloatspin[1],2)+pow(tryfloatspin[2],2)))
			if "Lev2" in h5dir[x]:
				spins_lev2.append(math.sqrt(pow(tryfloatspin[0],2)+pow(tryfloatspin[1],2)+pow(tryfloatspin[2],2)))
			if "Lev3" in h5dir[x]:
				spins_lev3.append(math.sqrt(pow(tryfloatspin[0],2)+pow(tryfloatspin[1],2)+pow(tryfloatspin[2],2)))
			if "Lev4" in h5dir[x]:
				spins_lev4.append(math.sqrt(pow(tryfloatspin[0],2)+pow(tryfloatspin[1],2)+pow(tryfloatspin[2],2)))
			if "Lev5" in h5dir[x]:
				spins_lev5.append(math.sqrt(pow(tryfloatspin[0],2)+pow(tryfloatspin[1],2)+pow(tryfloatspin[2],2)))
		#Find value of relaxed-mass1 element in metadata.txt
		#and add to an array of the masses.
		if "relaxed-mass1" in line:
			for l in line.split():
				try:
					tryfloatmass = float(l)
				except ValueError:
					pass
			if "Lev1" in h5dir[x]:
				mass_lev1.append(tryfloatmass)
			if "Lev2" in h5dir[x]:
				mass_lev2.append(tryfloatmass)
			if "Lev3" in h5dir[x]:
				mass_lev3.append(tryfloatmass)
			if "Lev4" in h5dir[x]:
				mass_lev4.append(tryfloatmass)
			if "Lev5" in h5dir[x]:
				mass_lev5.append(tryfloatmass)

       # t_common_horizon.append(time_comhorizon)
                    
        #Get the COM drift info from scri.SpEC and split it.
        #cominfo = SpEC.com_motion(h5dir[x])           
        #time = cominfo[0]
        #pos = cominfo[1]
        
        #Need to find corresponding avg com drift for the total simulation
	blockPrint()
	avgcominfo = SpEC.estimate_avg_com_motion(h5dir[x])
	enablePrint()
	avgpos = avgcominfo[0]
	avgvel = avgcominfo[1]

	#Find the element in the array that's closest to the common horizon time, and append the
	#magnitude to the COM position at common horizon array.
        #idx = (np.abs(time - time_comhorizon)).argmin()
        #pos_common_horizon = pos[idx]

        #Rename paths in h5dir to only include the section ".../Lev#"
        name = h5dir[x]
        name = name[:-12]
        h5dir[x] = name

	#Save the data to the appropriate Lev-designated arrays
	if "Lev1" in h5dir[x]:
		avgcom_pos_lev1.append(math.sqrt(pow(avgpos[0],2)+pow(avgpos[1],2)+pow(avgpos[2],2)))
		#com_pos_common_horizon_lev1.append(math.sqrt(pow(pos_common_horizon[0],2)+pow(pos_common_horizon[1],2)
		#					     +pow(pos_common_horizon[2],2)))
		avgcom_vel_lev1.append(math.sqrt(pow(avgvel[0],2)+pow(avgvel[1],2)+pow(avgvel[2],2)))
		h5dir_lev1.append(h5dir[x])
	if "Lev2" in h5dir[x]:
		avgcom_pos_lev2.append(math.sqrt(pow(avgpos[0],2)+pow(avgpos[1],2)+pow(avgpos[2],2)))
		#com_pos_common_horizon_lev2.append(math.sqrt(pow(pos_common_horizon[0],2)+pow(pos_common_horizon[1],2)
		#					     +pow(pos_common_horizon[2],2)))
		avgcom_vel_lev1.append(math.sqrt(pow(avgvel[0],2)+pow(avgvel[1],2)+pow(avgvel[2],2)))
		h5dir_lev2.append(h5dir[x])
	if "Lev3" in h5dir[x]:
		avgcom_pos_lev3.append(math.sqrt(pow(avgpos[0],2)+pow(avgpos[1],2)+pow(avgpos[2],2)))
		#com_pos_common_horizon_lev3.append(math.sqrt(pow(pos_common_horizon[0],2)+pow(pos_common_horizon[1],2)
		#+pow(pos_common_horizon[2],2)))
		avgcom_vel_lev1.append(math.sqrt(pow(avgvel[0],2)+pow(avgvel[1],2)+pow(avgvel[2],2)))
		h5dir_lev3.append(h5dir[x])
	if "Lev4" in h5dir[x]:
		avgcom_pos_lev4.append(math.sqrt(pow(avgpos[0],2)+pow(avgpos[1],2)+pow(avgpos[2],2)))
		#com_pos_common_horizon_lev4.append(math.sqrt(pow(pos_common_horizon[0],2)+pow(pos_common_horizon[1],2)
		#					     +pow(pos_common_horizon[2],2)))
		avgcom_vel_lev1.append(math.sqrt(pow(avgvel[0],2)+pow(avgvel[1],2)+pow(avgvel[2],2)))
		h5dir_lev4.append(h5dir[x])
	if "Lev5" in h5dir[x]:
		avgcom_pos_lev5.append(math.sqrt(pow(avgpos[0],2)+pow(avgpos[1],2)+pow(avgpos[2],2)))
		#com_pos_common_horizon_lev5.append(math.sqrt(pow(pos_common_horizon[0],2)+pow(pos_common_horizon[1],2)
		#					     +pow(pos_common_horizon[2],2)))
		avgcom_vel_lev1.append(math.sqrt(pow(avgvel[0],2)+pow(avgvel[1],2)+pow(avgvel[2],2)))
		h5dir_lev5.append(h5dir[x])


    #Plot the mass on the x axis with two curves - one for the avg com drift and the other for the 
    #com drift at time of common horizon.
   # plt.scatter(mass_lev1, avgcom_pos_lev1, color = 'r',marker = "o", s = 10, label = "Avg com drift- Lev1")
   # plt.scatter(mass_lev1, com_pos_common_horizon_lev1, color = 'r', marker = "s",  s = 10, label = "COM Drift at common horizon")
    plt.scatter(mass_lev1, avgcom_vel_lev1, color = 'r', marker ="s", s=10, label = "Lev1")

    #plt.scatter(mass_lev2, avgcom_pos_lev2, color = 'g',marker = "o",s = 10,  label = "Avg com drift- Lev2")
   # plt.scatter(mass_lev2, com_pos_common_horizon_lev2, color = 'g', marker = "s",  s = 10, label = "COM Drift at common horizon")    
    plt.scatter(mass_lev2, avgcom_vel_lev2, color = 'g', marker ="s", s=10, label = "Lev2")

    #plt.scatter(mass_lev3, avgcom_pos_lev3, color = 'b',marker = "o", s = 10, label = "Avg com drift- Lev3")
    #plt.scatter(mass_lev3, com_pos_common_horizon_lev3, color = 'b', marker = "s", s = 10, label = "COM Drift at common horizon")
    plt.scatter(mass_lev3, avgcom_vel_lev3, color = 'b', marker ="s", s=10, label = "Lev3")

    #plt.scatter(mass_lev4, avgcom_pos_lev4, color = 'k',marker = "o", s = 10, label = "Avg com drift- Lev4")
   # plt.scatter(mass_lev4, com_pos_common_horizon_lev4, color = 'k', marker = "s", s = 10, label = "COM Drift at common horizon")
    plt.scatter(mass_lev4, avgcom_vel_lev4, color = 'k', marker ="s", s=10, label = "Lev4")

    #plt.scatter(mass_lev5, avgcom_pos_lev5, color = 'm',marker = "o", s=10, label = "Avg com drift- Lev5")
    #plt.scatter(mass_lev5, com_pos_common_horizon_lev5, color = 'm', marker = "s", s=10, label = "COM Drift at common horizon")
    plt.scatter(mass_lev5, avgcom_vel_lev5, color = 'm', marker ="s", s=10, label = "Lev5")
    
    plt.grid(b=True, which = "minor", color = 'g', linestyle = "-")
    plt.title("Relaxed mass 1 vs COM velocity")
    plt.xlabel("Relaxed mass 1")
    plt.ylabel("COM velocity")

    ax = plt.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
    plt.legend(loc = "center left", bbox_to_anchor = (1,0.5), prop = {'size':7})

    plt.savefig(args.filename[0]+"_VelocityAndMass.pdf")
    plt.clf()
    plt.close()  
 
    #Plot the spins on the x axis with two curves - one for the avg com drift and the other for the 
    #com drift at time of common horizon.
    #plt.scatter(spins_lev1, avgcom_pos_lev1, color = 'r',marker = "o", s=10, label = "Avg com drift- Lev1")
    #plt.scatter(spins_lev1, com_pos_common_horizon_lev1, color = 'r', marker = "s",s=10, label = "COM Drift at common horizon")
    plt.scatter(spins_lev1, avgcom_vel_lev1, color = 'r', marker = 'o', s=10, label = 'Lev1')

    #plt.scatter(spins_lev2, avgcom_pos_lev2, color = 'g',marker = "o", s=10, label = "Avg com drift- Lev2")
    #plt.scatter(spins_lev2, com_pos_common_horizon_lev2, color = 'g', marker = "s", s=10, label = "COM Drift at common horizon")    
    plt.scatter(spins_lev2, avgcom_vel_lev2, color = 'g', marker = 'o', s=10, label = 'Lev2')

    #plt.scatter(spins_lev3, avgcom_pos_lev3, color = 'b',marker = "o", s=10,label = "Avg com drift- Lev3")
    #plt.scatter(spins_lev3, com_pos_common_horizon_lev3, color = 'b', marker = "s", s=10, label = "COM Drift at common horizon")
    plt.scatter(spins_lev3, avgcom_vel_lev3, color = 'b', marker = 'o', s=10, label = 'Lev3')

    #plt.scatter(spins_lev4, avgcom_pos_lev4, color = 'k',marker = "o", s=10,label = "Avg com drift- Lev4")
    #plt.scatter(spins_lev4, com_pos_common_horizon_lev4, color = 'k', marker = "s", s=10, label = "COM Drift at common horizon")
    plt.scatter(spins_lev4, avgcom_vel_lev4, color = 'k', marker = 'o', s=10, label = 'Lev4')

    #plt.scatter(spins_lev5, avgcom_pos_lev5, color = 'm',marker = "o", s=10, label = "Avg com drift- Lev5")
    #plt.scatter(spins_lev5, com_pos_common_horizon_lev5, color = 'm', marker = "s", s=10, label = "COM Drift at common horizon")
    plt.scatter(spins_lev5, avgcom_vel_lev5, color = 'm', marker = 'o', s=10, label = 'Lev5')

    plt.grid(b=True, which = "minor", color = 'g', linestyle = "-")
    plt.title("Relaxed spin 1 magnitude vs COM velocity")
    plt.xlabel("Relaxed spin 1 magnitude")
    plt.ylabel("COM velocity")

    ax = plt.subplot(111)
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
    plt.legend(loc = "center left", bbox_to_anchor = (1,0.5), prop = {'size':7})

    plt.savefig(args.filename[0]+"_VelocityAndSpin.pdf")
    plt.clf()
    plt.close()

    #Save mass, spin and COM drift data in a seperate file.
    f = open(args.filename[0]+".txt",'w')
    f.write('Run Name and Lev	AVG COM position magnitude    AVG com velocity magnitude      Mass             Spin\n')
    f.write('\n')

    for x in range(0, len(h5dir_lev1)):
	    f.write(h5dir_lev1[x] +"      "+repr(avgcom_pos_lev1[x])+"          "+repr(avgvel_lev1[x])+"             "+repr(mass_lev1[x])+"           "+repr(spins_lev1[x])+"\n")
    for x in range(0, len(h5dir_lev2)):
	    f.write(h5dir_lev2[x] +"      "+repr(avgcom_pos_lev2[x])+"          "+repr(avgvel_lev2[x])+"             "+repr(mass_lev2[x])+"           "+repr(spins_lev2[x])+"\n")
    for x in range(0, len(h5dir_lev3)):
	    f.write(h5dir_lev3[x] +"      "+repr(avgcom_pos_lev3[x])+"          "+repr(avgvel_lev3[x])+"             "+repr(mass_lev3[x])+"           "+repr(spins_lev3[x])+"\n")
    for x in range(0, len(h5dir_lev4)):
	    f.write(h5dir_lev4[x] +"      "+repr(avgcom_pos_lev4[x])+"          "+repr(avglev_lev4[x])+"             "+repr(mass_lev4[x])+"           "+repr(spins_lev4[x])+"\n")
    for x in range(0, len(h5dir_lev5)):
	    f.write(h5dir_lev5[x] +"      "+repr(avgcom_pos_lev5[x])+"          "+repr(avglev_lev5[x])+"             "+repr(mass_lev5[x])+"           "+repr(spins_lev5[x])+"\n")


    f.close()

if __name__=="__main__":
    main()
