#!/usr/bin/env python

"""
Make corner plots for:
1. x,y,z components of the avg COM velocity and mass
2. " and spin1 and spin2
3. avg COM velocity magnitude, mass, spin1, spin2

The outputs are these 3 corner plots.
"""

import os
import sys
import math
import argparse
import numpy as np
import matplotlib.pyplot as plt
import corner

def blockPrint():
#prevent printing to screen or to files
	sys.stdout = open(os.devnull, 'w')

def enablePrint():
#enable printing
	sys.stdout = sys.__stdout__

def main():

    #Parse Arguments from command line
    parser = argparse.ArgumentParser(
        description = "Make corner plots of avg COM velocity, mass ratio, and spin values.")
    parser.add_argument("--dir", nargs = "+", required = "True",
                        help = "Directory in which to start looking for the metadata.txt and Horizons.h5 files.")
    parser.add_argument("--filename", nargs = "+", required = "True", dest = "filename",
                        help = "Include a filename  for the corner plots. The option is the plot file name - do not include pdf!")

    args = parser.parse_args()
	
    import scri.SpEC as SpEC
    	
#Find and store all of the directories within the given directory that contain Horizons.h5.
#os.path.join combines the directory name d and file name x for simplicity when calling the 
#scri functions. This also checks that the Horizons.h5 files are present and not just linked.
    h5dir = [os.path.join(d,x)
	     for d, dirs, files in os.walk(args.dir[0], topdown=True) 
	     for x in files if x.endswith("Horizons.h5") and os.path.isfile(d+'/'+x)]

#Do the same for metadata.txt. Presumably, the same directories that contain Horizons.h5 will
#also contain metadata.txt
    metadir = []

    for x in range(0,len(h5dir)):
        metaname = h5dir[x]
        metaname = metaname[:-11]
        metadir.append(metaname+"metadata.txt")

#Create arrays for spins, masses, and average COM position values.\
    spin1 = []
    spin2 = []
    spin_eff = []
    avgcom_pos = []
    avgcom_vel = []
    avgcom_vx = []
    avgcom_vy = []
    avgcom_vz = []
    q = []

#Run over all files that were in the specified starting directory
    for x in range(0,len(h5dir)):

        #Find the lines in metadata.txt that have the common horizon time data
        with open(metadir[x],'r') as metafile:
            for line in metafile:
		if "relaxed-dimensionless-spin1" in line:
			tryfloatspin = []
			for l in line.split():
                            try:
                                if l[-1]==',':
                                    tryfloatspin.append(float(l[:-1]))
                                else:
                                    tryfloatspin.append(float(l))
                            except ValueError:
                                pass
                        spin1.append(math.sqrt(pow(tryfloatspin[0],2)+pow(tryfloatspin[1],2)+pow(tryfloatspin[2],2)))
		if "relaxed-dimensionless-spin2" in line:
			tryfloatspin = []
			for l in line.split():
                            try:
                                if l[-1]==',':
                                    tryfloatspin.append(float(l[:-1]))
                                else:
                                    tryfloatspin.append(float(l))
                            except ValueError:
                                pass
                        spin2.append(math.sqrt(pow(tryfloatspin[0],2)+pow(tryfloatspin[1],2)+pow(tryfloatspin[2],2)))
		#Find value of relaxed-mass1 element in metadata.txt
		#and add to an array of the masses.
		if "relaxed-mass1" in line:
                    for l in line.split():
                        try:
                            tryfloatmass = float(l)
                        except ValueError:
                            pass
                    mass1 = tryfloatmass
                if "relaxed-mass2" in line:
                    for l in line.split():
                        try:
                            tryfloatmass = float(l)
                        except ValueError:
                            pass
                    mass2 = tryfloatmass

        #Calculate spin effective and mass ratio q
        spin_eff.append((mass1*spin1[-1] + mass2*spin2[-1])/(mass1+mass2))
        q.append(mass1/mass2)

        #Need to find corresponding avg com drift for the total simulation
	blockPrint()
	avgcominfo = SpEC.estimate_avg_com_motion(h5dir[x])
	enablePrint()
	avgpos = avgcominfo[0]
	avgvel = avgcominfo[1]

        #Rename paths in h5dir to only include the section ".../Lev#"
        name = h5dir[x]
        name = name[:-12]
        h5dir[x] = name

	#Save the data to the appropriate arrays
        avgcom_pos.append(math.sqrt(pow(avgpos[0],2)+pow(avgpos[1],2)+pow(avgpos[2],2)))
        avgcom_vel.append(math.sqrt(pow(avgvel[0],2)+pow(avgvel[1],2)+pow(avgvel[2],2)))
        avgcom_vx.append(avgvel[0])
        avgcom_vy.append(avgvel[1])
        avgcom_vz.append(avgvel[2])

    #Plot vx, vy, vz, and q as a corner plot
    data1 = np.vstack([avgcom_vx, avgcom_vy, avgcom_vz, q]).T
    figure = corner.corner(data1, labels=['v_x','v_y','v_z','q'], show_titles=True)

    figure.savefig(args.filename[0]+'_VelocityAndMass.pdf')

    #Plot vx, vy, vz, and the spins as a corner plot
    data2 = np.vstack([avgcom_vx, avgcom_vy, avgcom_vz, spin1, spin2]).T
    figure = corner.corner(data2, labels = ['v_x','v_y','v_z','Chi_1','Chi_2'], show_titles=True)

    figure.savefig(args.filename[0]+'_VelocityAndSpins.pdf')

    #Plot |v|, |spin1|, |spin2|, q as a corner plot.
    data3 = np.stack([avgcom_vel, spin1, spin2, q]).T
    figure = corner.corner(data3, labels = ['|v|','chi_1','chi_2','q'], show_titles=True)

    figure.savefig(args.filename[0]+'_VelocityMag_qAndChi.pdf')

    #Plot |p|, |v|, |spin_eff|, q as a corner plot
    data4 = np.stack([avgcom_pos, avgcom_vel, spin_eff, q]).T
    figure = corner.corner(data4, labels = ['|p|', '|v|', 'chi_eff','q'], show_titles=True)

    figure.savefig(args.filename[0]+'_PosVelSpinsQ.pdf')

    #Save mass, spin and COM drift data in a seperate file.
    f = open(args.filename[0]+".txt",'w')
    f.write('Run Name and Lev	AVG COM position magnitude    AVG com velocity magnitude      q             Spin1               Spin2             Spin_eff\n')
    f.write('\n')

    for x in range(0, len(h5dir)):
        f.write(h5dir[x] +"      "+repr(avgcom_pos[x])+"          "+repr(avgcom_vel[x])+"             "+repr(q[x])+"           "+repr(spin1[x])+"        "+repr(spin2[x])+"          "+repr(spin_eff[x])+"\n")
 
    f.close()

if __name__=="__main__":
    main()
